This project is tested with BrowserStack.
[BrowserStack](https://www.browserstack.com/)

### # Содержание
* [Работа с гит](#Работа-с-git)
* [Основные подходы разработки](#Основные-подходы-разработки)
* [UI](#UI)
* [Генерация моделей](#Генерация-моделей)
* [Сборка](#Сборка)
* [Запуск](#Запуск)
* [Локализация Poeditor](#Локализация-Poeditor)
* [Публикация](#Публикация)

# Работа с git

## Ветки

Формат веток следующий - "{номер задачи}-{краткое описание задачи (слова через - kebab case)} (1047-documentation-refactoring - пример)

## Коммиты

Формат коммитов следующий - "{номер задачи} {WIP если работа в процессе либо DONE если работа завершена} {текст сообщения коммита}"

Язык коммитов - Русский

## Проверка пул реквестов и веток

До оформления пул реквеста нужно убедиться
* что все условия из CHECKLIST.md соблюдены
* что проект полностью отформатирован и в проекте нет замечаний от анализатора.

В пул реквесте будет произведена проверка на это.

# Основные подходы разработки

> Подходы архитектуры, которой мы придерживаемся, описаны в статье https://medium.com/@kirsapps/my-vision-of-flutter-app-architecture-a80fd8824568
>
> Ее следует изучить, там немного.

## Язык разработки

Язык разработки русский. Всю документацию, все комментарии, коммиты, мердж реквесты, ивенты логгера и т.д. пишем на русском языке.

## Переиспользование кода

Для лучшего переиспользования кода нужно изучить CODE_KIT.md

## Инъекция зависимостей
В проекте все репозитории, дата провайдеры и иные зависимости находятся в классе DependencyContainer.

Для добавления новых зависимостей вы можете
* объявить переменную и передать зависимость в конструктор (в этом случае все приложение будет использовать один инстанс зависимости)
* объявить метод или геттер, который возвращает завивимость (в случае если нужно чтобы при новых вызовах возвращалась новая зависимость)

сам файл с контейнером находится по пути lib/root/models/dependency_container.dart

если будете передавать зависимость через конструктор, то инициализация контейнера происходит в файле lib/root/models/runner.dart, класс AppRunner начиная с 40 строчки.

## Репозитории и модели данных

### Общий принцип

Получение данных осуществляется через репозитории и дата провайдеры.

Общая схема выглядит так

> блок -> репозиторий -> дата провайдер

Для репозиториев и дата провайдеров создается интерфейс (абстрактный класс) с заглавной I в названии (пример IShoppingCartRepository).

Блок общается с репозиторием, который предоставляет конечную модель необходимых данных и знает только про репозиторий.

Сам репозиторий может использовать дата провайдеры.

Дата провайдеры получают сырые данные и возвращают дто модель, основанную на этих данных.

### Пример

Нам нужно получить данные корзины покупок.

Сначала нам нужно создать 2 модели данных:
* ShoppingCart - модель данных, которую будет возвращать репозиторий. В этой модели данных делаем factory ShoppingCart.fromDto(ShoppingCartDto dto) - по аналогии с fromJson.
* ShoppingCartDto - модель данных, которую будет возвращать дата провайдер. В этой модели данных с использованием json_serializable генерируем метод fromJson.

Мы создаем:
* IShoppingCartRepository - интерфейс репозитория для загрузки корзины. В нем создаем метод loadShoppingCart, который возвращает модель данных ShoppingCart.
* IShoppingCartDataProvider - интерфейс дата провадера для загрузки корзины. В нем создаем метод loadShoppingCart, который возвращает модель данных ShoppingCartDto.

И получается такая реализация
1) блок вызывает метод loadShoppingCart у класса имплементирующего IShoppingCartRepository (далее - shoppingCartRepository)
2) внутри shoppingCartRepository вызывается метод loadShoppingCart у класса имплементирующего IShoppingCartDataProvider (далее - shoppingCartDataProvider)
3) внутри shoppingCartDataProvider происходит запрос на бэкенд, ответ бэкенда передается в factory ShoppingCartDto.fromJson и как резальтат выполнения метода возвращается эта модель.
4) внутри shoppingCartRepository получем дто модель ShoppingCartDto с данными и передаем ее в factory ShoppingCart.fromDto, получаем модель и возвращаем ее.

#### Моковые репозитории

В примере выше также можно реализовать моковый репозиторий MockShoppingCartRepository.

Если моковый репозиторий не делает никакие запросы, то дата провайдер для него делать необязательно.

## Обработка ошибок

Пока нет необходимости в обратном, действует общий принцип обработки ошибок на уровне блока, а именно никакие репозитории и дата провайдеры не содержат в себе конструкции try catch.

В блоке, все методы, которые теоретически и даже с очень маленькой долей вероятности могут закончиться ошибкой, мы выполняем внутри try catch.

Это касается и вызовов апи/методов в ui, например когда вызываем выбор файла из пакета и т.д.

В catch мы получем также стектрейс -> catch(e, s) (e - это ошибка, s - это стектрейс)

Внутри catch мы реагируем на ошибку, а также обязательно передаем событие в логгер с описанием того какая опрерация провалилась и указанием на блок и метод, в котором ошибка произошла.


>  logger.error(
> DeLogRecord(
>
> 'Не удалось загрузить записи логгера', - описание
>
> name: 'DeveloperLogBloc -> _onRecordsFetched', - блок и метод
>
> error: e, - ошибка
>
> stackTrace: s, - стектрейс
> ),
> );

## Локализация
Для получения локализации в проекте есть extension AppLocalizationsX над BuildContext.

Находится по пути lib/l10n/l10n.dart, для получения локализации нужно импортировать этот файл и вызвать геттер l10n -> context.l10n

## Роуты

В приложении используется пакет [auto_route](https://pub.dev/packages/auto_route).

Рекомендуется прочитать документацию по ссылке выше.

Для указания роутов мы используем файл lib/parts/router/router_part.dart.

* Если необходимо чтобы страница открывалась с боттом баром внизу (внутренняя навигация вкладок), то роуты нужно указывать в параметре children 5 роутов, которые используются как вкладки (пути discover, actions, search, chats_list, user).

  > Eсли нужно чтобы экран открывался в нескольких вкладках, то новый роут нужно добавить в children в каждую вкладку. Например, чтобы роут открылся во вкладках discover, actions, chats_list, вы в этих 3 роутах в параметре children указываете этот роут.

* Если нужно чтобы роут открывался на весь экран без боттом бара (без внутренней навигации вкладок), то вы указываете новый роут в параметре routes класса CustomAutoRouter. Так вы укажете верхнеуровневый роут.

**После указания новых роутов нужно запустить кодогенерацию.**

## Добавление зависимостей

Зависимости добавляем с точной версией без крышечки.

В алфавитном порядке.

## Документирование

Документирование осуществляется на русском языке.

Много писать не нужно, одного, двух предложений достаточно. Чтобы у другого человека после прочтения было понимание того, что это такое и для чего нужно.

Подходы по документации указаны здесь [effective dart](https://dart.dev/guides/language/effective-dart/documentation). Желательно прочитать все.

Для переиспользования документации для класса и его конструктора можно использовать template и macro, пример - ChannelSubscribeButtonPart lib/parts/channel_subscribe_button/channel_subscribe_button_part.dart

## Получение Access Token

Токен можно получить из репозитория аутентификации.

## Создание форм

Формы создаем с использованием пакета flutter_form_builder. Вам нужно изучить его документацию чтобы эффективно его использовать.

## Именование стейтов и ивентов в блоке

Стейты и ивенты в блоке именуем в соответствии с [конвенцией](https://bloclibrary.dev/#/blocnamingconventions)

## Показ уведомлений

Логгер может показывать уведомления. Для того чтобы показать уведомление, в модель данных DeLogRecord нужно передать параметр notificationType:
* warning - Предупреждение
* error - Ошибка
* info - Информация

Текст уведомления формируется следующим образом - если есть ошибка, то текст будет взят из ошибки функцией extractErrorMessage, если ошибки нет, то показан будет параметр message модели данных DeLogRecord.

Пример показа уведомления - lib/code_kit/deep_links/deep_links_handler_cubit.dart

## Работа с enum

Если вам нужно определить какое значение enum содержит переменная, то используйте switch case конструкцию. Для enum она подходит лучше потому что при добавлении новых элементов места, где этот новый элемент не обрабатывается (в switch case конструкции), будут подсвечены.

# UI

## Темы, стили и цвета

### Тема
В проекте используется настроенная стандартная тема, находится в файле lib/ui_kit/theme/theme.dart.

При использовании стандартных виджетов, которые зависят от настроек темы, нужно вносить изменения в этом файле.

> Получение темы реализовано через геттер theme у BuildContext -> context.theme

### Цвета

файл с настрояками цветов находтся по пути lib/ui_kit/colors/colors.dart.

В нем указаны цвета из дизайна.

> Получение цветов реализовано через геттер colors у ThemeData -> context.theme.colors

### Стили текста

Стили текста указаны в файле темы, находятся в параметре textTheme класса ThemeData.

Стили указаны в соответствии с дизайном.


## SVG изображения

Чтобы показать SVG изображение, следует использовать виджет AppSvgImage.

## Загрузка картинок из сети

Загрузка и показ картинок осуществляется через виджет AppImageLoader. Работает так же как и Image.network и ему подобные виджеты.

Внутри этот виджет будем показывать в зависимости от результата:
* изображение
* шиммер при загрузке изображения
* ошибку

## Shimmer

Этот эффект реализуется двумя виджетами:
* AppShimmer - создает сам эффект
* AppShimmerContainer - который просто болванка белого цвета. под воздействием AppShimmer на нем виден шиммер эффект.

> **Создание и показ виджета AppShimmer затратно и желательно использовать всего один виджет там где это возможно.**

Принцип реализации следующий:
1. расставляем болванки AppShimmerContainer там где нужно, имитируя элементы загруженного экрана
2. выше по дереву, над всеми болванками, размещаем AppShimmer, который один будет создавать для всех болванок анимацию.

## Animated Switcher

Для эффекта AnimatedSwitcher мы используем виджет AppAnimatedSwitcher.

## Верстка кнопок

Если элемент в дизайне похож на кнопку и должен реагировать на нажатия, то мы используем TextButton, OutlinedButton или ElevatedButton как основной виджет и его кастомизируем.

## Ввод текста с клавиатуры

Если на экране присутствуют поля ввода текста с клавиатуры, то весь экран нужно обернуть в виджет AppTapUnfocus, чтобы при тапе мимо поля фокус пропадал. Это нужно для более удобного скрытия клавиатуры.

Также изучите параметр keyboardType у TextField и подставляйте наиболее подходящий, чтобы пользователям было удобнее вводить данные.

## Настройка ide

**idea**
* нужно убедиться что в Editor -> CodeStyle в поле Hard wrap at стоит число 80
* также в Editor -> CodeStyle -> Dart Line length должен быть равен 80

**vscode**
* нужно установить плагин https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig

# Генерация моделей

flutter pub run build_runner build --delete-conflicting-outputs

# Сборка

flutter build apk -t lib/main_dev.dart - dev окружение

flutter build apk -t lib/main_prod.dart - prod окружение

flutter build apk -t lib/main_mock.dart - mock окружение

# Запуск

flutter run -t lib/main_dev.dart - dev окружение

flutter run -t lib/main_prod.dart - prod окружение

flutter run -t lib/main_mock.dart - mock окружение

# Локализация Poeditor

Переводы на разные языки нам сейчас не нужны, но потом может потребоваться, например, для Казахстана
Если не делать сразу, то потом очень сложно

[Poeditor.com](https://poeditor.com) используется для хранения и управления строковыми ресурсами.
Используем бесплатную подписку сервиса.

Для загрузки самых последних строковых ресурсов из службы используйте
```
python l10n.py import
```

Для загрузки локальных строковых ресурсов в службу используйте команду
```
python l10n.py export
```
При загрузке **не удаляются, но переопределяются** уже существующие ресурсы, а только добавляются новые данные в сервис.

# Публикация

Разные Bundle ID
* app.chat.android - Android
* app.chat.apple - Ios

Наш сайт ???

iOS ???

Android ???

# Фоновое создание постов

Используется настройка Foreground Upload Timeout in seconds. Установили значение в 1 час = 3600 секунд.

В Info.plist

```
<key>FUTimeoutInSeconds</key>
    <integer>3600</integer>
```

В AndroidManifest

```
<meta-data android:name="com.bluechilli.flutteruploader.UPLOAD_CONNECTION_TIMEOUT_IN_SECONDS" android:value="3600" />
```

groupId - Используется для идентификации поста, который создаём. Нужно для отслеживания и управления загрузкой в списке в ленте.
oldGuid - guid, который создаём в приложение для файла (имя файла). После загрузки на сервер guid может измениться (если такое название уже будет на билайне) и в ответе от билайна придёт иной. Если не сохранять старый guid, то не сможем управлять коллекцией загрузок.

## Под рукой

Примеры json аннотаций

@JsonSerializable()
class BeelineObject {
  @JsonKey(name: 'id')
  final String id;

затем перегенерация командой
`flutter pub run build_runner build --delete-conflicting-outputs`

### Fluffy docs

<p align="center">
  <a href="https://matrix.to/#/#fluffychat:matrix.org" target="new">Join the community</a> - <a href="https://metalhead.club/@krille" target="new">Follow me on Mastodon</a> - <a href="https://hosted.weblate.org/projects/fluffychat/" target="new">Translate FluffyChat</a> - <a href="https://gitlab.com/ChristianPauly/fluffychat-website" target="new">Translate the website</a> - <a href="https://fluffychat.im" target="new">Website</a> - <a href="https://gitlab.com/famedly/famedlysdk" target="new">Famedly Matrix SDK</a> - <a href="https://famedly.com/kontakt">Server hosting and professional support</a>
 </p>


FluffyChat is an open source, nonprofit and cute matrix messenger app. The app is easy to use but secure and decentralized.

## Features

- Send all kinds of messages, images and files
- Voice messages
- Location sharing
- Push notifications
- Unlimited private and public group chats
- Public channels with thousands of participants
- Feature rich group moderation including all matrix features
- Discover and join public groups
- Dark mode
- Custom themes
- Hides complexity of Matrix IDs behind simple QR codes
- Custom emotes and stickers
- Spaces
- Compatible with Element, Nheko, NeoChat and all other Matrix apps
- End to end encryption
- Emoji verification & cross signing
- And much more...

# Installation

Please visit our website for installation instructions:

https://fluffychat.im

# How to build

Please visit our Wiki for build instructions:

https://gitlab.com/famedly/fluffychat/-/wikis/How-To-Build


# Special thanks

* <a href="https://github.com/fabiyamada">Fabiyamada</a> is a graphics designer from Brasil and has made the fluffychat logo and the banner. Big thanks for her great designs.

* <a href="https://github.com/advocatux">Advocatux</a> has made the Spanish translation with great love and care. He always stands by my side and supports my work with great commitment.

* Thanks to MTRNord and Sorunome for developing.

* Also thanks to all translators and testers! With your help, fluffychat is now available in more than 12 languages.

* <a href="https://github.com/googlefonts/noto-emoji/">Noto Emoji Font</a> for the awesome emojis.

* <a href="https://github.com/madsrh/WoodenBeaver">WoodenBeaver</a> sound theme for the notification sound.

* The Matrix Foundation for making and maintaining the [emoji translations](https://github.com/matrix-org/matrix-doc/blob/main/data-definitions/sas-emoji.json) used for emoji verification, licensed Apache 2.0
